--1. Use floating points with decimal because it does not to be very precise. 
--2. Characters is the appropriate data type. It is a good idea to seperate first and last name because it gives more query options. It also simplifies the dataand reduces the chance of people entering spaces where they shouldn't and capital letters where they shouldn't.
--3. There will be a format error because it does have a day. Also if it was being transformed into the ISO standard then it would not be following the correct format. 

-- Notes
-- Use intergers when possible
-- Decimals that need to be exact use numeric
-- Float types save space but are less precise
-- You can specify how many place values maximum a data set can have by (x,y) x is how many places before the decimal point and y is how many places after the decimal place. 
-- Using the cast function in a select statement allows you to change the data type. Useful for when you retriev a number and want to use it with a sting. 
-- timstamp tracks date and time
-- date tracks date
-- time tracks time - remember to keep track of time zones
-- interval tracks time periods in quantitativly


