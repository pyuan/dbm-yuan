SELECT animal, name, age
FROM local_zoo
ORDER BY animal ASC;

SELECT animal, name, age
FROM local_zoo
WHERE name LIKE 'A%'
AND age <= 7;

SELECT animal, scientific_name, name, age
FROM local_zoo
WHERE age BETWEEN 0 AND 100
ORDER BY age DESC;


-- Notes
-- I did not follow the book exactly. I wanted to use the table from chapter one. So for things like date, I replaced with other where statements.
-- There is no need for , at the end of SELECT or FROM.

-- LIKE STATEMENTS are useful for case sensitive situations.
-- LIKE is case sensitive.
-- ILIKE is psql insensitive
-- USE % as filler
-- There are many WHERE clauses
-- =, <>, !=, <, >, <=, >=, BETWEEN, IN, LIKE, ILIKE, NOT
-- ORDER BY [column name] DESC or ASC
-- ORDER BY sorts data
-- To add more clauses use ADD
-- To say or use OR

