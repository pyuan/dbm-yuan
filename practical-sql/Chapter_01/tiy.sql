DROP TABLE IF EXISTS local_zoo;
CREATE TABLE local_zoo (
	animal             TEXT,
	scientific_name    TEXT,
	name               TEXT,
	age                 INT
);

INSERT INTO local_zoo (animal, scientific_name, name, age)
VALUES  ('lion',   'Panthera leo', 'John',      '10'),
	('owl',    'Strigiformes', 'Adrian',    '6'), 
	('frog',   'Anura',        'Francesco', '2'),
	('turtle', 'Testudines',   'Peter',     '130');

-- Notes
-- When using drop table before create table remember to add ; at the end.
-- For the last column dont put an , at the end. 
