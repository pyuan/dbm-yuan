
 INSERT INTO books(title, author, publish_date, checked_out, LCCN, book_copy, LC) 
 VALUES ('The relational model for database management', 'E. F. Codd', 1990, false, '89006793', 1, 'QA76.9.D3 C626 1990'),
        ('The C Answer Book', 'Clovis L. Tondo', 1989, false, '88025134', 1, 'QA76.73.C15 K47 1988'),
        ('The C Programming Language, Second Edition', 'Brian  W. Kernighan', 1988, false, '77028983', 1, 'QA76.73.C15 K47 1988')
        ('The C Programming Language, Second Edition', 'Brian  W. Kernighan', 1988, false, '77028983', 2, 'QA76.73.C15 K47 1988')
        ('The C Programming Language, Second Edition', 'Brian  W. Kernighan', 1988, false, '77028983', 3, 'QA76.73.C15 K47 1988')
        ('The Elements of Artificial Intelligence Using Common Lisp, Second Edition', 'Steven L. Tanimoto', 1995, false, '95003160', 1,),
        ('ANSI Common Lisp', 'Paul Graham', 1996, false, '95045017', 1),
        ('Foundations of Analysis', 'Edmund Landau', 1951, false, '51002456', 1)
	;
        
INSERT INTO patrons(first_name, last_name, patron_id)	
VALUES  ('Peter', 'Yuan', 986220),
	('Chase', 'Dillard', 991212)
	;

INSERT INTO loans(date_out, date_in, patron_id_patrons, LCCN, book_copy)
VALUES	('3/10/19', '', '986220', '89006793', '1')
	;
