--Create Schema
CREATE SCHEMA IF NOT EXISTS libraries;

--Create patrons
DROP TABLE IF EXISTS libraries.patrons CASCADE;
CREATE TABLE libraries.patrons(
	first_name	TEXT,
	last_name	TEXT,
	patron_id	INT,
	CONSTRAINT users_pk PRIMARY KEY (patron_id)
);

--Create books
DROP TABLE IF EXISTS libraries.books CASCADE;
CREATE TABLE libraries.books(
	title		TEXT,
	author		TEXT,
	publish_date	INT,
	checked_out	BOOLEAN,
	LCCN		INT,
	book_copy	SERIAl,
	CONSTRAINT books_pk PRIMARY KEY (LCCN, book_copy)
);

--Create loans
DROP TABLE IF EXISTS libraries.loans CASCADE;
CREATE TABLE libraries.loans(
	date_out	TEXT,
	date_in		TEXT,
	patron_id       INT,
	LCCN		INT,
	book_copy	SERIAL
);

--Connects the pk and fk together
ALTER TABLE libraries.loans
ADD CONSTRAINT patrons_fk FOREIGN KEY(patron_id)
REFERENCES libraries.patrons (patron_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE libraries.loans
ADD CONSTRAINT books_fk FOREIGN KEY(LCCN, book_copy)
REFERENCES libraries.books (LCCN, book_copy) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE libraries.loans ADD constraint loan_uq UNIQUE (patron_id);
