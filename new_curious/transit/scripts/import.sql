CREATE SCHEMA IF NOT EXISTS import;

DROP TABLE IF EXISTS events cascade; 
DROP TABLE IF EXISTS teams cascade;
DROP TABLE IF EXISTS targets cascade;
DROP TABLE IF EXISTS spass_types cascade;
DROP TABLE IF EXISTS requests cascade;
DROP TABLE IF EXISTS event_types cascade;

DROP TABLE IF EXISTS import.master_plan;
CREATE TABLE import.master_plan (
    start_time_utc      TEXT,
    duration            TEXT,
    date                TEXT,
    team                TEXT,
    spass_type          TEXT,
    target              TEXT,
    request_name        TEXT,
    library_definition  TEXT,
    title               TEXT,
    description         TEXT
);
