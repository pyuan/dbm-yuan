-- First try gets a whole bunch of data.

SELECT targets.description AS target,
time_stamp,
title
FROM events
inner join targets ON target_id=targets.id
;

-- Second try. Tries to filter down to flyby.
SELECT targets.description AS target,
time_stamp,
title
FROM events
inner join targets ON target_id=targets.id
WHERE title LIKE '%flyby%' or '%fly by%';

-- Third try. Fixes second try using or and like. 
SELECT targets.description AS target,
time_stamp,
title
FROM events
inner join targets ON target_id=targets.id
WHERE title LIKE '%flyby%' 
OR title LIKE '%fly by%';

-- Fourth try. Uses regex to filter out. Looks for only T.
SELECT targets.description AS target,
time_stamp,
title
FROM events
inner join targets ON target_id=targets.id
WHERE title ~* '^T\d.*? flyby';

-- Fifth try. Uses improved regex to filter down even more. Looks for T and then number and letter.
SELECT targets.description AS target,
time_stamp,
title
FROM events
inner join targets ON target_id=targets.id
WHERE title ~* '^T[A-Z0-9_].*? flyby';

-- Sixth try. Adds another coloumn and columns.
SELECT targets.description AS target, 
	event_types.description AS event,
	time_stamp,
	time_stamp::DATE AS DATE,
	title
FROM events
LEFT JOIN targets ON target_id=targets.id
LEFT JOIN event_types ON event_type_id=event_types.id
WHERE title ilike '%flyby%'
OR title ilike '%fly by%';
