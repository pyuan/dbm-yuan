DROP materialized VIEW IF EXISTS enceladus_events;
CREATE materialized VIEW enceladus_events AS
SELECT
  events.id,
  events.title,
  events.description,
  event_types.description AS event,
  events.time_stamp::DATE AS DATE,
  events.time_stamp,
  to_tsvector(concat(
  events.description, ' ',
  events.title)
) AS search
FROM events
INNER JOIN event_types
ON event_types.id = events.event_type_id
WHERE target_id=28
ORDER BY time_stamp;

CREATE INDEX idx_event_search
ON enceladus_events USING GIN(search);
SELECT id, DATE,title
FROM enceladus_events
WHERE search @@ to_tsquery('closest');

SELECT time_stamp, title
FROM events
WHERE time_stamp::DATE='2009-11-02'
ORDER BY time_stamp;

SELECT (time_stamp at time zone 'UTC'),
title
FROM events
WHERE (time_stamp at time zone
'UTC')::DATE='2009-11-02'
ORDER BY time_stamp;


